from twisted.internet.protocol import Protocol, ClientFactory
from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol
from twisted.internet import reactor
import threading


global host, port, serveraddr
host = 'localhost'
port = 8123
serveraddr = '[' + host + ':' + str(port) + ']'

class ClientEcho(Protocol):
    flag = True
    def connectionMade(self):
        print 'connented to server' + serveraddr
        self.sendMessage('Nice to meet you!')

    def sendMessage(self, msg):
        tmsg = "MESSAGE %s" % msg
        print 'send to ' + serveraddr + ': ' + tmsg
        self.transport.write(tmsg)

    def dataReceived(self, data):
        print "receive from " + serveraddr + ":" + data
        print 'input: (bye to bye)'
        if self.flag:
            cmd = raw_input()
            if cmd == 'bye':
                self.flag = False
                reactor.callLater(1, self.transport.loseConnection)
            else:
                self.sendMessage(cmd)


class EchoClientFactory(ClientFactory):
    global host, port
    def startedConnecting(self, connector):
        print 'Connecting to ' + serveraddr

    def buildProtocol(self, addr):
        print 'Connected'
        return ClientEcho()

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection ', reason

    def clientConnectionFailed(self, connector, reason):
        print 'Connection Failed ', reason

# def inputThread(protocol):
#     #for i in range(3):
#     while True:
#         print 'input:'
#         cmd = raw_input()
#         protocol.sendMessage(cmd)
#         #protocol.transport.write(cmd)


p = ClientEcho()
d = reactor.connectTCP(host, port, EchoClientFactory())
reactor.run()