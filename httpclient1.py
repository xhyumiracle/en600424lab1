from twisted.internet.protocol import Protocol, ClientFactory
from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol
from twisted.internet import reactor, stdio
import threading
from twisted.protocols.basic import LineReceiver
import sys


global hoststr, httpprotocol
host = 'localhost'
port = 8123
hoststr = host + ':' + str(port)


class HttpProtocol(Protocol):
    global hoststr
    flag = True
    httphead = 'GET {} HTTP/1.{}\r\nHost: {}\r\n\r\n'

    def connectionMade(self):
        self.log(hoststr, 'con')
        # cmd = raw_input()
        # while (cmd == ''): cmd = raw_input()
        # self.sendMessage(self.sendHttpGet(cmd, '1')) # second arg is x for HTTP 1.x

        # self.sendMessage('Nice to meet you!')

    def sendMessage(self, data):
        self.log(hoststr, 'snd', data)
        self.transport.write(data)

    def sendHttpGet(self, filepath, host = hoststr, version = '1'):
        self.sendMessage(self.httphead.format(filepath, version, host)) # second arg is x for HTTP 1.x


    def dataReceived(self, data):
        self.log(hoststr, 'rcv', data)
        sys.stdout.write('>>>')
        sys.stdout.flush()
        self.hookDataReceived()


        # filepath = raw_input()
        # print 'input: (bye to bye)'
        # if self.flag:
        #     cmd = raw_input()
        #     if cmd == '':
        #         self.flag = False
        #         reactor.callLater(1, self.transport.loseConnection)
        #     else:
        #         self.sendMessage(cmd)
        # self.sendHttpGet(filepath)

    def log(self, server, type, data = ''):
        serveraddr = '[' + server + ']'
        if type == 'con':
            logstr = '[CON]' + 'connected to ' + serveraddr
        elif type == 'rcv':
            logstr = '[RCV]' + serveraddr + '\n' + data
        elif type == 'snd':
            logstr = '[SND]' + serveraddr + '\n' + data
        elif type == 'req':
            logstr = '[REQ]' + serveraddr + data
        print logstr


class HttpFactory(ClientFactory):
    global host, port, httpprotocol

    def startedConnecting(self, connector):
        print 'Connecting to ' + hoststr

    def buildProtocol(self, addr):
        print 'Connected'
        return httpprotocol

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection ', reason

    def clientConnectionFailed(self, connector, reason):
        print 'Connection Failed ', reason

class StdIO(LineReceiver):
    from os import linesep as delimiter
    global httpprotocol
    delimiter = '\n'

    def connectionMade(self):
        self.transport.write('>>>')

    def lineReceived(self, line):
        httpprotocol.sendHttpGet(line)

# def inputThread(protocol):
#     #for i in range(3):
#     while True:
#         print 'input:'
#         cmd = raw_input()
#         protocol.sendMessage(cmd)
#         #protocol.transport.write(cmd)


httpprotocol = HttpProtocol()
d = reactor.connectTCP(host, port, HttpFactory())
stdio.StandardIO(StdIO())
reactor.run()