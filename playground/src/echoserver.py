from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ServerEndpoint
from playground.twisted.endpoints import GateServerEndpoint

global clientaddr
port = 8123
clientaddr = ''

class EchoServer(Protocol):
    def connectionMade(self):
        global clientaddr
        clientip = self.transport.getPeer().host
        clientport = self.transport.getPeer().port
        clientaddr = '[' + clientip + ':' + str(clientport) + ']'
        print clientaddr + 'has connected'

    def dataReceived(self, data):
        global clientaddr
        print clientaddr + 'receive: ' + data
        self.sendMessage(data)
        print clientaddr + "send: " + data

    def sendMessage(self, msg):
        tmsg = "MESSAGE %s" % msg
        self.transport.write(tmsg)


class EchoFactory(Factory):
    def buildProtocol(self, addr):
        return EchoServer()

#endpoint = TCP4ServerEndpoint(reactor, port)
#endpoint.listen(EchoFactory())
endpoint = GateServerEndpoint(reactor, 8123, "127.0.0.1", 19090)
endpoint.listen(EchoFactory())

reactor.run()
