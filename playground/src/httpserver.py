import re

from twisted.internet import reactor
from twisted.internet.protocol import Protocol, Factory

from rip import RIPStack
from playground.twisted.endpoints import GateServerEndpoint

global client, root
port = 8124
#root = 'D:\Study\Network Security\lab1\En600424Lab1\html'
root = './html'
indexhostfix = ['.html', '.htm', '.php', '']


class HttpServer(Protocol):
    def handleHttpHead(self, head):
        pattern = re.compile('GET (.*) (?:HTTP/1.0|HTTP/1.1)\r\nHost: .*\r\n')
        #pattern = re.compile('GET (.*) HTTP/1.1\r\nHost: .*\r\n\r\n')
        reqs = pattern.findall(head)
        return reqs

    def getIndex(self, hostfixes, len, i):
        if i >= len:
            return '[Err] 404 not found!'
        try:
            obj = open(root + '/index' + hostfixes[i], 'r')
            content = obj.read()
            obj.close()
        except Exception, err:
            content = self.getIndex(hostfixes, len, i+1)
            print err
        return content

    def getResource(self, file):
        try:
            obj = open(root + file, 'r')
            content = obj.read()
            obj.close()
        except Exception, err:
            print err
            content = self.getIndex(indexhostfix, len(indexhostfix), 0)
        return content

    def connectionMade(self):
        global client
        clientip = self.transport.getPeer()[0]
        clientport = self.transport.getPeer()[1]
        client = clientip + ':' + str(clientport)
        self.log(client, 'con')

    def dataReceived(self, data):
        global client
        self.log(client, 'rcv', data)
        reqs = self.handleHttpHead(data)
        for req in reqs:
            self.log(client, 'req', req)
            self.sendMessage(self.getResource(req))

    def sendMessage(self, data):
        global client
        self.transport.write(data)
        self.log(client, 'snd', data)

    def log(self, client, type, data = ''):
        clientaddr = '[' + client + ']'
        if type == 'con':
            logstr = '[CON]' + clientaddr + 'has connected'
        elif type == 'rcv':
            logstr = '[RCV]' + clientaddr + '\n' + data
        elif type == 'snd':
            logstr = '[SND]' + clientaddr + '\n' + data
        elif type == 'req':
            logstr = '[REQ]' + clientaddr + data
        print logstr


class HttpFactory(Factory):
    def buildProtocol(self, addr):
        return HttpServer()

#endpoint = TCP4ServerEndpoint(reactor, port)
#endpoint = GateServerEndpoint.CreateFromConfig(reactor, 101,"xhyumiracle")
endpoint = GateServerEndpoint(reactor, port, "127.0.0.1", 19090, networkStack=RIPStack)
endpoint.listen(HttpFactory())
reactor.run()
