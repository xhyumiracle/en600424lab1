'''
Created on Oct 18, 2016

@author: xhyu
'''
from twisted.internet.protocol import Protocol, Factory
from zope.interface.declarations import implements
from twisted.internet.interfaces import ITransport, IStreamServerEndpoint
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING
from playground.network.common.Protocol import StackingTransport, \
    StackingProtocolMixin, StackingFactoryMixin


class RipMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "RIPStack"
    MESSAGE_VERSION = "1.0"

    BODY = [("data", STRING)]


class RipTransport(StackingTransport):
    def __init__(self, lowerTransport):
        StackingTransport.__init__(self, lowerTransport)

    def write(self, data):
        ripMessage = RipMessage()
        ripMessage.data = data
        self.lowerTransport().write(ripMessage.__serialize__())


class RipProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):
        self.buffer = ""

    def connectionMade(self):
        higherTransport = RipTransport(self.transport)
        self.makeHigherConnection(higherTransport)

    def dataReceived(self, data):
        self.buffer += data
        try:
            ripMessage, bytesUsed = RipMessage.Deserialize(data)
            self.buffer = self.buffer[bytesUsed:]
        except Exception, e:
            # print "We had a deserialization error", e
            return

        self.higherProtocol() and self.higherProtocol().dataReceived(ripMessage.data)
        self.buffer and self.dataReceived("")


class RipFactory(StackingFactoryMixin, Factory):
    protocol = RipProtocol


ConnectFactory = RipFactory
ListenFactory = RipFactory