'''
Created on Oct 18, 2016

@author: xhyu
'''
from twisted.internet.protocol import Protocol, Factory
from zope.interface.declarations import implements
from twisted.internet.interfaces import ITransport, IStreamServerEndpoint
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING
from playground.network.common.Protocol import StackingTransport, \
    StackingProtocolMixin, StackingFactoryMixin
from playground.network.message.StandardMessageSpecifiers import UINT4, OPTIONAL, STRING, DEFAULT_VALUE, LIST, BOOL1
from RIPConfig import HANDSHAKE_STEP0, HANDSHAKE_STEP1, DATA_TRANSPORT, DEBUG

class RipMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "RIPStack"
    MESSAGE_VERSION = "1.0"

    BODY = [
        # ("sequence_number", UINT4),

        ("acknowledgement_number", UINT4, OPTIONAL),

        ("signature", STRING, DEFAULT_VALUE("")),

        ("certificate", LIST(STRING), OPTIONAL),

        # ("sessionID", STRING),

        ("acknowledgement_flag", BOOL1, DEFAULT_VALUE(False)),

        ("close_flag", BOOL1, DEFAULT_VALUE(False)),

        ("sequence_number_notification_flag", BOOL1,
         DEFAULT_VALUE(False)),

        ("reset_flag", BOOL1, DEFAULT_VALUE(False)),

        ("data", STRING, DEFAULT_VALUE("")),

        ("OPTIONS", LIST(STRING), OPTIONAL)
    ]


class RipTransport(StackingTransport):
    def __init__(self, lowerTransport):
        if DEBUG:
            print "rip transport init"
        StackingTransport.__init__(self, lowerTransport)

    def write(self, data):
        ripMessage = RipMessage()
        ripMessage.data = data
        self.lowerTransport().write(ripMessage.__serialize__())


class RipClientProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):
        if DEBUG:
            print "rip protocol init"
            print "logprefix", self.logPrefix()
        self.__buffer = ""
        self.__status=HANDSHAKE_STEP0

    def __send_1st_packet(self):
        self.__sendMessage("hello1")

    def __check_2nd_packet(self, data):
        ripMessage, bytesUsed = RipMessage.Deserialize(data)
        data = ripMessage.data
        self.__log('rcv', data)
        if data != 'hello2':
            return False
        return True

    def __send_3rd_packet(self):
        self.__sendMessage("hello3")

    def __handshake(self, data=""):
        if self.__status == HANDSHAKE_STEP0:
            self.__send_1st_packet()
            self.__status = HANDSHAKE_STEP1
        elif self.__status == HANDSHAKE_STEP1:
            success = self.__check_2nd_packet(data)
            if not success:
                self.__log('err', "check 2nd failed, data:\n" + data)
                # TODO: how to handle this when handshake failed?
                self.__status = HANDSHAKE_STEP0
            self.__send_3rd_packet()
            self.__status = DATA_TRANSPORT
            # handshake success
            self.makeHigherConnection(self.__higherTransport)

    def connectionMade(self):
        self.__log('con')
        self.__higherTransport = RipTransport(self.transport)
        self.__handshake()

    def dataReceived(self, data):
        if self.__status != DATA_TRANSPORT:
            self.__handshake(data)
            return

        self.__buffer += data
        try:
            ripMessage, bytesUsed = RipMessage.Deserialize(data)
            self.__buffer = self.__buffer[bytesUsed:]
        except Exception, e:
            # print "We had a deserialization error", e
            return
        self.higherProtocol() and self.higherProtocol().dataReceived(ripMessage.data)
        self.__buffer and self.dataReceived("")

    def __sendMessage(self, data):
        self.__log('snd', data)
        self.__higherTransport.write(data)

    def __log(self, type, data = ''):
        ripstr = '{rip}'
        logstr = '{rip}Null log, how can this be ??? You did write out a bug dude!'
        if type == 'con':
            logstr = ripstr + '{CON}'
        elif type == 'rcv':
            logstr = ripstr + '{RCV}' + '\n' + data
        elif type == 'snd':
            logstr = ripstr + '{SND}' + '\n' + data
        elif type == 'err':
            logstr = ripstr + '{ERROR}' + data
        print logstr


class RipServerProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):
        print "rip protocol init"
        self.__buffer = ""
        self.__status = HANDSHAKE_STEP0

    def connectionMade(self):
        self.__log('con')
        self.__higherTransport = RipTransport(self.transport)

    def __check_1st_packet(self, data):
        ripMessage, bytesUsed = RipMessage.Deserialize(data)
        data = ripMessage.data
        self.__log('rcv', data)
        if data != 'hello1':
            return False
        return True

    def __send_2nd_packet(self):
        self.__sendMessage("hello2")

    def __check_3rd_packet(self, data):
        ripMessage, bytesUsed = RipMessage.Deserialize(data)
        data = ripMessage.data
        self.__log('rcv', data)
        if data != 'hello3':
            return False
        return True

    def __handshake(self, data=""):
        if self.__status == HANDSHAKE_STEP0:
            success = self.__check_1st_packet(data)
            if not success:
                self.__log('err', "check 1st failed, data:" + data)
                # TODO: how to handle failure?
                self.__status = HANDSHAKE_STEP0
                return False
            self.__send_2nd_packet()
            self.__status = HANDSHAKE_STEP1
        elif self.__status == HANDSHAKE_STEP1:
            success = self.__check_3rd_packet(data)
            if not success:
                self.__log('err', "check 3rd failed, data:" + data)
                return False
            self.__status = DATA_TRANSPORT
            # handshake success
            self.makeHigherConnection(self.__higherTransport)

    def dataReceived(self, data):
        if self.__status != DATA_TRANSPORT:
            self.__handshake(data)
            return

        self.__buffer += data
        try:
            ripMessage, bytesUsed = RipMessage.Deserialize(data)
            self.__buffer = self.__buffer[bytesUsed:]
        except Exception, e:
            # print "We had a deserialization error", e
            return

        self.higherProtocol() and self.higherProtocol().dataReceived(ripMessage.data)
        self.__buffer and self.dataReceived("")

    def __sendMessage(self, data):
        self.__log('snd', data)
        self.__higherTransport.write(data)

    def __log(self, type, data = ''):
        ripstr = '{rip}'
        logstr = '{rip}Null log, how can this be ??? You did write out a bug dude!'
        if type == 'con':
            logstr = ripstr + '{CON}'
        elif type == 'rcv':
            logstr = ripstr + '{RCV}' + '\n' + data
        elif type == 'snd':
            logstr = ripstr + '{SND}' + '\n' + data
        elif type == 'err':
            logstr = ripstr + '{ERROR}' + data
        print logstr


class RipClientFactory(StackingFactoryMixin, Factory):
    protocol = RipClientProtocol


class RipServerFactory(StackingFactoryMixin, Factory):
    protocol = RipServerProtocol

ConnectFactory = RipClientFactory
ListenFactory = RipServerFactory