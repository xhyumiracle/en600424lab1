# Lab1 #

### Introduction

* This Checkpoint 1&2 are written under Windows, Checkpoint 3 is under Ubuntu 16.04
* Please do check the tag: submission-2 as my finial submission.
* I pushed my lab2 project to this repo.. I realized that it's not a good thing, I will separate them ASAP. But for now, please check tag[submission-2] instead of master branch.

### Checkpoints and File paths

* Checkpoint 1: EN600424Lab1/echoclient.py & echoserver.py (Windows version)
* Checkpoint 2: EN600424Lab1/httpclient.py & httpserver.py (Windows version)
* Checkpoint 3: EN600424Lab1/playground/src/httpclient.py & httpserver.py (Linux version)
* Files for the first 2 checkpoints are the Windows version, which means the root path in them are Windows style and getPeer() (in some files) probably won't work correctly in Linux.